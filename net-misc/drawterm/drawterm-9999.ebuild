# Copyright 1999-2021 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils git-r3

DESCRIPTION="connect to Plan 9 CPU servers from other operating systems"
HOMEPAGE="http://drawterm.9front.org/"
LICENSE="9base MIT"
SLOT="9front"
EGIT_REPO_URI="https://github.com/9fans/drawterm"
KEYWORDS="~x86 ~amd64 ~arm"
IUSE=""

DEPEND="x11-base/xorg-server"

src_compile() {
	export CONF=unix
	default
}

src_install() {
	dobin drawterm
	dodoc README.md
}

